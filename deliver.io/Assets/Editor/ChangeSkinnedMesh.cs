﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class UpdateSkinnedMeshWindow : EditorWindow
{
    [MenuItem("Window/Update Skinned Mesh Bones")]
    public static void OpenWindow()
    {
        var window = GetWindow<UpdateSkinnedMeshWindow>();
        window.titleContent = new GUIContent("Skin Updater");
    }
    private SkinnedMeshRenderer thisSkin;
    private SkinnedMeshRenderer targetSkin;
    private Transform targetHip;
    private void OnGUI()
    {
        thisSkin = EditorGUILayout.ObjectField("This Skin", thisSkin, typeof(SkinnedMeshRenderer), true) as SkinnedMeshRenderer;
        targetSkin = EditorGUILayout.ObjectField("Target Skin", targetSkin, typeof(SkinnedMeshRenderer), true) as SkinnedMeshRenderer;
        targetHip = EditorGUILayout.ObjectField("Target Root bone", targetHip, typeof(Transform), true) as Transform;

        bool have = (targetSkin != null && thisSkin != null && targetHip != null);
        GUI.enabled = have;

        if (GUILayout.Button("Update Skinned Mesh Renderer"))
        {
            Undo.RegisterCompleteObjectUndo(thisSkin, "Changed skinned mesh");
            Undo.FlushUndoRecordObjects();

            Dictionary<string, Transform> boneMap = new Dictionary<string, Transform>();
            foreach (Transform bone in targetSkin.bones)
            {
                boneMap[bone.name] = bone;
            }
            Transform[] boneArray = thisSkin.bones;

            for (int i = 0; i < boneArray.Length; i++)
            {
                string boneName = boneArray[i].name;
                if (false == boneMap.TryGetValue(boneName, out boneArray[i]))
                {
                    Debug.LogError("failed to get bone: " + boneName);
                    Debug.Break();
                }
            }
            thisSkin.bones = boneArray;
            thisSkin.rootBone = targetHip;

            Debug.Log("Changed skin");
        }
    }
}