﻿using UnityEngine;
using UnityEditor;

public class RepeatPositioner : EditorWindow
{
    public Vector3 MovePos = Vector3.zero;

    [MenuItem("Tools/Repeat Positioner")]
    static void ShowWindow()
    {
        GetWindow<RepeatPositioner>();
    }

    private void OnGUI()
    {
        MovePos = EditorGUILayout.Vector3Field("Move Position", MovePos);

        GUILayout.Space(10);

        if(GUILayout.Button("Move"))
        {
            if(Selection.transforms.Length > 0)
            {
                GameObject last = null;

                Undo.IncrementCurrentGroup();
                Undo.SetCurrentGroupName("Repeat Positioner");
                int undoID = Undo.GetCurrentGroup();

                bool duplicate = true;

                if(duplicate)
                {
                    foreach(Transform t in Selection.transforms)
                    {
                        Object prefabRoot = PrefabUtility.GetCorrespondingObjectFromOriginalSource(t.gameObject);

                        if(prefabRoot != null)
                        {
                            GameObject g = PrefabUtility.InstantiatePrefab(prefabRoot) as GameObject;
                            Transform tran = g.transform;
                            tran.SetParent(t.parent);
                            Undo.RegisterCreatedObjectUndo(tran.gameObject, "duplicated repeat object");
                            tran.localPosition = t.localPosition + MovePos;
                            tran.localRotation = t.localRotation;

                            last = tran.gameObject;
                        }
                    }
                }
                else
                {
                    foreach (Transform t in Selection.transforms)
                    {
                        Undo.RecordObject(t, "Positioned");
                        t.localPosition += MovePos;
                    }
                }

                Selection.activeGameObject = last;
                Undo.CollapseUndoOperations(undoID);
            }
        }

        if(Selection.transforms.Length == 2)
        {
            if(GUILayout.Button("Get Distance"))
            {
                MovePos = Selection.transforms[0].position - Selection.transforms[1].position;
            }
        }
    }

}
