﻿using UnityEngine;

public class PrefabManager : MonoBehaviour
{
    [SerializeField] Transform PlatePrefab = null;
    [SerializeField] Transform[] FoodGroup = null;

    public static Transform platePrefab { get; private set; }
    public static Transform[] foodGroup { get; private set; }

    private void Awake()
    {
        platePrefab = PlatePrefab;
        foodGroup = new Transform[FoodGroup.Length];
        foodGroup = FoodGroup;
    }
}
