﻿using UnityEngine;
using UnityEngine.UI;

public class Ranking : MonoBehaviour
{
    public static Ranking instance { get; private set; }

    [SerializeField] RectTransform contentHolder = null;

    [SerializeField] Transform nameHolderPrefab = null;

    private void Awake()
    {
        instance = this;

        for(int i = 0; i < contentHolder.childCount; i++)
        {
            Destroy(contentHolder.GetChild(i).gameObject);
        }
    }

    public void ShowPlayerName(string[] names, int playerID)
    {
        Vector2 r = contentHolder.sizeDelta;
        contentHolder.sizeDelta = new Vector2(r.x, names.Length * 100 + (names.Length + 1) * 50);

        int counter = 0;
        for(int i = 0; i < names.Length; i++)
        {
            counter++;
            RectTransform t = Instantiate(nameHolderPrefab).GetComponent<RectTransform>();
            t.SetParent(contentHolder);
            t.localScale = Vector3.one;

            t.GetChild(0).GetChild(0).GetComponent<Text>().text = counter.ToString();
            t.GetChild(1).GetComponent<Text>().text = names[i];

            if (i == playerID)
                t.GetChild(1).GetComponent<Text>().color = Color.red;
        }
    }
}
