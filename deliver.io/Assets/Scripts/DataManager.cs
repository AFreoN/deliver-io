﻿using UnityEngine;
using UnityEngine.UI;

public class DataManager : MonoBehaviour
{
    public static int currentLevel { get; private set; }


    const string tutorialKey = "Tutorial";
    const string playerNameKey = "PlayerName";

    const string levelKey = "Level";
    const string coinKey = "Coin";

    const string headGear = "Head";
    const string upperGear = "Upper";
    const string lowerGear = "Lower";

    #region Settings
    const string vibrationKey = "Vibration";
    const string joystickKey = "JoystickType";
    public static int vibration = 1;
    public static int dynamicJoystick = 1;
    #endregion

    [SerializeField] bool SetLevel = false;
    [SerializeField] int Level = 1;

    [SerializeField] bool unlockAllSkins = false;

    private void Awake()
    {
        if(PlayerPrefs.HasKey(levelKey))
        {
            LoadLevel();
        }
        else
        {
            PlayerPrefs.SetInt(tutorialKey, 0);
            PlayerPrefs.SetString(playerNameKey, "Player");
            PlayerPrefs.SetInt(levelKey, 1);
            PlayerPrefs.SetInt(coinKey, 0);

            PlayerPrefs.SetInt(headGear, 0);
            PlayerPrefs.SetInt(upperGear, 0);
            PlayerPrefs.SetInt(lowerGear, 0);
            InitSkinsData();

            PlayerPrefs.SetInt(vibrationKey, 1);
            PlayerPrefs.SetInt(joystickKey, 1);
        }

        if(SetLevel)
        {
            currentLevel = Level;
            PlayerPrefs.SetInt(levelKey, currentLevel);
        }

        SetPlayerName(LoadPlayerName);

        vibration = PlayerPrefs.GetInt(vibrationKey,1);
        dynamicJoystick = PlayerPrefs.GetInt(joystickKey,1);
        //SaveCoins(500);
        //PlayerPrefs.SetInt(coinKey, 30);

        if(unlockAllSkins)
        {
            for (int i = 0; i < 9; i++)
            {
                PlayerPrefs.SetInt(headGear + i, 1);
                PlayerPrefs.SetInt(upperGear + i, 1);
                PlayerPrefs.SetInt(lowerGear + i, 1);
            }
        }
    }

    public static void disableTutorial() { PlayerPrefs.SetInt(tutorialKey, 1); }
    public static bool isTutorialNeeded => PlayerPrefs.GetInt(tutorialKey) == 0;

    public static void SaveLevel()
    {
        currentLevel = (currentLevel + 1) % (LevelController.TotalLevels + 1);
        currentLevel = currentLevel != 0 ? currentLevel : 1;
        PlayerPrefs.SetInt(levelKey, currentLevel);
    }

    public static int LoadLevel()
    {
        currentLevel = PlayerPrefs.GetInt(levelKey, 1);
        return currentLevel;
    }

    void InitSkinsData()
    {
        PlayerPrefs.SetInt(headGear + "0", 1);
        for (int i = 1; i < 9; i++)
            PlayerPrefs.SetInt(headGear + i, 0);

        PlayerPrefs.SetInt(upperGear + "0", 1);
        for (int i = 1; i < 9; i++)
            PlayerPrefs.SetInt(upperGear + i, 0);

        PlayerPrefs.SetInt(lowerGear + "0", 1);
        for (int i = 1; i < 9; i++)
            PlayerPrefs.SetInt(lowerGear + i, 0);
    }

    public static int[] loadHeadSkinData()
    {
        int[] result = new int[9];
        for (int i = 0; i < 9; i++)
            result[i] = PlayerPrefs.GetInt(headGear + i);

        return result;
    }

    public static void unlockHeadSkin(int id)
    {
        PlayerPrefs.SetInt(headGear + id, 1);
    }

    public static int[] loadUpperSkinData()
    {
        int[] result = new int[9];
        for (int i = 0; i < 9; i++)
            result[i] = PlayerPrefs.GetInt(upperGear + i);

        return result;
    }

    public static void unlockUpperSkin(int id)
    {
        PlayerPrefs.SetInt(upperGear + id, 1);
    }

    public static int[] loadLowerSkinData()
    {
        int[] result = new int[9];
        for (int i = 0; i < 9; i++)
            result[i] = PlayerPrefs.GetInt(lowerGear + i);

        return result;
    }

    public static void unlockLowerSkin(int id)
    {
        PlayerPrefs.SetInt(lowerGear + id, 1);
    }

    public static bool isHeadSkinUnlocked(int id) => PlayerPrefs.GetInt(headGear + id) == 1;

    public static void SaveHeadGear(int id)     //Save the current head gear
    {
        PlayerPrefs.SetInt(headGear, id);
    }

    public static int HeadGear { get { return PlayerPrefs.GetInt(headGear, 0); } }  //Return the current head gear

    public static bool isUpperSkinUnlocked(int id) => PlayerPrefs.GetInt(upperGear + id) == 1;

    public static void SaveUpperGear(int id)
    {
        PlayerPrefs.SetInt(upperGear, id);
    }

    public static int UpperGear { get { return PlayerPrefs.GetInt(upperGear, 0); } }    //Return the current upper gear

    public static bool isLowerSkinUnlocked(int id) => PlayerPrefs.GetInt(lowerGear + id) == 1;

    public static void SaveLowerGear(int id)
    {
        PlayerPrefs.SetInt(lowerGear, id);
    }

    public static int LowerGear { get { return PlayerPrefs.GetInt(lowerGear, 0); } }    //Return the current lower gear

    //Add the coins in to the current coins
    public static void SaveCoins(int amount)
    {
        int r = PlayerPrefs.GetInt(coinKey);
        PlayerPrefs.SetInt(coinKey, r + amount);
    }

    public static int Coins => PlayerPrefs.GetInt(coinKey);     //Load the amount of coins

    public void SavePlayerName(InputField field)
    {
        if (field.text.Length <= 0)
        {
            field.text = LoadPlayerName.ToString();
            return;
        }

        PlayerPrefs.SetString(playerNameKey, field.text);
        SetPlayerName(LoadPlayerName);
    }

    public static string LoadPlayerName => PlayerPrefs.GetString(playerNameKey);

    void SetPlayerName(string s)
    {
        PlayerController g = PlayerController.instance;
        if (g != null)
        {
            g.name = s;
            g.GetComponent<PlayerCanvas>().nameText.text = s;
        }
    }

    public static void ToggleVibration(int id)
    {
        PlayerPrefs.SetInt(vibrationKey, id);
        vibration = id;
    }

    public static void ToggleJoystick(int id)
    {
        PlayerPrefs.SetInt(joystickKey, id);
        dynamicJoystick = id;
    }
}
