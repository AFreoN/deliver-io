﻿

public static class ControlValues
{
    public const float viewAngle = 60;
    public const float interactionDelay = .25f;

    public const float plateStackDistance = .15f;

    //This value is percentage based
    public const float speedReductionPerPlate = .12f;   //.15f //wave 2 -> .075f   //

    //These values are additive, not in percents
    public const float scaleIncPerPlate = 0.035f;     //.1f
    public const float speedIncPerPlate = 0.1f;    //.25f
    public const float powerIncPerPlate = 10;       //30
    public const float massIncPerPlate = 4;        //10

    //Player Speed Booster
    public const float speedBoostMultiplier = 1.5f;    //This is value multiplied with the speed of the player while in boost mode
    public const float speedBoostTime = 1f;     //This is the amount of time, boost is going to last
    public const float runAnimSpeed = 1.1f;
}
