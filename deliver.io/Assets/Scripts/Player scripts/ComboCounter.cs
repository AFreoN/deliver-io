﻿using UnityEngine;
using UnityEngine.UI;

public class ComboCounter : MonoBehaviour
{
    public static ComboCounter instance;

    [SerializeField] Text comboText = null;
    Animator comboAnim = null;

    [SerializeField] Text remText = null;
    Animator remAnim = null;

    const string hitClipName = "spawn";
    const string remClipName = "rem";

    PowerHandler ph = null;
    int totalPlatesRequired = 10;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        comboAnim = comboText.GetComponent<Animator>();
        //comboText.text = "Hit <color=#FFFF00>x" + hitCombo.ToString() + "</color>";
        comboAnim.enabled = false;

        remAnim = remText.GetComponent<Animator>();

        ph = GameObject.FindGameObjectWithTag(TagsLayers.playerTag).GetComponent<PlayerController>().powerHandler;
        totalPlatesRequired = LevelController.instance.PlatesForEachLevel[DataManager.currentLevel - 1];
    }

    public void showCombo(int platesCount)
    {
        Transform t = comboText.transform;
        Vector3 pos = SpawnsTables.currentDeliverTable.transform.position + Vector3.up + Vector3.right * .5f;
        pos = Camera.main.WorldToScreenPoint(pos);
        t.position = pos;

        float r = Random.Range(10, 25.1f);
        int mul = Utilities.roll(2) == true ? 1 : -1;
        r *= mul;
        t.localRotation = Quaternion.Euler(0, 0, r);
        comboText.text = "+"  + platesCount.ToString();
        comboAnim.enabled = false;
        comboAnim.enabled = true;
        comboAnim.Play(hitClipName, 0, 0);

        spawnRem();
    }

    void spawnRem()
    {
        int remCount = totalPlatesRequired - ph.platesDelivered;
        if(remCount > 0)
        {
            string s = remCount != 1 ? " Plates remaining" : " Plate remaining";
            remText.text = "<color=#FFFF00>" + remCount.ToString() + "</color>" + s;
            remAnim.Play(remClipName,0,0);
        }
    }
}
