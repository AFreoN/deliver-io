﻿using UnityEngine;

[RequireComponent(typeof(Animator))][RequireComponent(typeof(PlayerController))]
public class PlayerAnimator : MonoBehaviour
{
    Animator anim;
    PlayerController pc;

    [SerializeField] RuntimeAnimatorController danceController = null;

    const string run_Param = "run";
    const string carry_Param = "carry";
    public const string dodge_ClipName = "Dodging Back";
    public static float dodge_ClipLength { get; private set; }

    bool finished = false;

    PLAYERSTATE playerState = PLAYERSTATE.Idle;

    void Start()
    {
        anim = GetComponent<Animator>();
        pc = GetComponent<PlayerController>();

        foreach(AnimationClip clip in anim.runtimeAnimatorController.animationClips)
        {
            if (clip.name == dodge_ClipName)
            {
                dodge_ClipLength = clip.length;
            }
        }

        SetAnimValues(false, false);
    }

    private void Update()
    {
        if(GameManager.gameState == GameState.Win)
        {
            if (finished)
                return;
            else
            {
                anim.runtimeAnimatorController = danceController;
                int r = Random.Range(1, 4);
                anim.SetInteger("move", r);
                transform.forward = Vector3.back;
                finished = true;
            }
        }

        if(playerState != pc.playerState)
            switchAnimation();
    }

    void switchAnimation()
    {
        switch (pc.playerState)
        {
            case PLAYERSTATE.Idle:

                SetAnimValues(false, false);

                break;

            case PLAYERSTATE.Run:

                SetAnimValues(true, false);

                break;

            case PLAYERSTATE.CarryIdle:

                SetAnimValues(false, true);

                break;

            case PLAYERSTATE.CarryRun:

                SetAnimValues(true, true);

                break;

            case PLAYERSTATE.Dodged:

                SetAnimValues(false, false);

                break;
        }

        playerState = pc.playerState;
    }

    void SetAnimValues(bool run, bool carry)
    {
        resetAnimation(run_Param, run);
        resetAnimation(carry_Param, carry);
    }

    void resetAnimation(string s,bool isTrue = false)
    {
        if (finished)
            return;

        if (anim.GetBool(s) != isTrue)
            anim.SetBool(s, isTrue);
    }
}
