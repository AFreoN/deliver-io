﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance { get; private set; }
    Rigidbody rb = null;
    Joystick joyStick = null;

    Animator anim = null;
    float defaultAnimSpeed = 1;
    [HideInInspector] public PowerHandler powerHandler = null;

    [SerializeField] PlayerProperties playerProps = null;

    float movementSpeed = 0;
    float speedReductionPerPlate = 0;
    float minPlayerSpeed = 0;
    float minInputDistance = 0;

    //For Temporary Speed Boost
    float speedBoostMultiplier = 1;
    float minPlateForBoost = 1;
    Transform currentStealParticle = null;
    Transform currentBoostParticle = null;

    public PLAYERSTATE playerState { get; private set; }
    Coroutine currentDodgeRoutine = null;

    //For Plates
    float plateStackDistance = .1f;
    Transform platesHolder = null;
    [SerializeField] List<Transform> plates = new List<Transform>();
    [HideInInspector] public bool havePlates = false;

    private void Awake()
    {
        instance = this;

        rb = GetComponent<Rigidbody>();
        joyStick = GetComponent<Joystick>();
        anim = GetComponent<Animator>();
        powerHandler = new PowerHandler(transform, 1);
        playerState = PLAYERSTATE.Idle;
    }

    private void Start()
    {
        getValuesFromProps();
        platesHolder = transform.GetChild(0).Find("Plates Holder");
    }

    void getValuesFromProps()
    {
        movementSpeed = playerProps.playerSpeed;
        speedReductionPerPlate = ControlValues.speedReductionPerPlate;
        minPlayerSpeed = playerProps.minPlayerSpeed;

        minInputDistance = playerProps.minInputDistance;
        plateStackDistance = playerProps.plateStackDistance;
    }

    private void Update()
    {
        havePlates = plates.Count > 0 ? true : false;

        if(GameManager.gameState == GameState.InGame && playerState != PLAYERSTATE.Dodged && joyStick.minDisMoved(minInputDistance))
        {
            if (plates.Count != 0)
                playerState = PLAYERSTATE.CarryRun;
            else
                playerState = PLAYERSTATE.Run;
        }
        else if(playerState != PLAYERSTATE.Dodged)
        {
            if(plates.Count != 0)
            {
                playerState = PLAYERSTATE.CarryIdle;
            }
            else
                playerState = PLAYERSTATE.Idle;
        }
    }

    private void FixedUpdate()
    {
        if (GameManager.gameState == GameState.InGame && playerState != PLAYERSTATE.Dodged && joyStick.minDisMoved(minInputDistance))
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(Vector3.up * Joystick.angle), .4f);
        }
            switch (playerState)
        {
            case PLAYERSTATE.Idle:
                rb.velocity = Vector3.zero;
                break;

            case PLAYERSTATE.Run:
                rb.velocity = transform.forward * (movementSpeed + powerHandler.speed) * speedBoostMultiplier * 50 * Time.fixedDeltaTime;
                //Vector3 tempVec = transform.forward * (movementSpeed + powerHandler.speed) * speedBoostMultiplier * Time.fixedDeltaTime;
                //rb.MovePosition(Vector3.Lerp(transform.position, transform.position + tempVec, .2f));
                break;

            case PLAYERSTATE.CarryIdle:
                rb.velocity = Vector3.zero;
                break;

            case PLAYERSTATE.CarryRun:
                float normalVelocity = (movementSpeed + powerHandler.speed) * speedBoostMultiplier;
                float reduction = 1 - plates.Count * speedReductionPerPlate;        //Speed reduction by carrying plates
                float currentSpeed = Mathf.Clamp(normalVelocity * reduction, minPlayerSpeed, Mathf.Infinity);
                rb.velocity = transform.forward * currentSpeed * 50 * Time.fixedDeltaTime;
                break;

            case PLAYERSTATE.Dodged:
                rb.AddForce(transform.forward * -10 * 50 * Time.fixedDeltaTime);
                break;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag(TagsLayers.spawnPointTag))
        {
            PlateSpawner ps = collision.gameObject.GetComponent<PlateSpawner>();
            if(ps != null && ps.havePlate)
            {
                CarryPlate(ps);
            }
        }

        if(collision.gameObject.CompareTag(TagsLayers.tableTag))
        {
            if (plates.Count != 0 && collision.gameObject.GetComponent<Table>() == SpawnsTables.currentDeliverTable)
            {
                Vibration.Vibrate(40);

                powerHandler.updateScale(plates.Count);

                Transform prefab = ParticlesManager.sizeUpParticle;
                Instantiate(prefab, transform.position + Vector3.up, prefab.rotation).SetParent(transform);

                ProgressBar.increasePlates(powerHandler.platesDelivered);
                CameraController.current.ZoomOut(transform.lossyScale.y);

                if (plates.Count > 0)
                {
                    ComboCounter.instance.showCombo(plates.Count);
                    for(int i = 0; i < plates.Count; i++)
                        InGamepanel.instance.SpawnCoin(collision.transform.GetChild(1).position);
                }

                foreach (Transform t in plates)
                    Destroy(t.gameObject);

                plates.Clear();
                SpawnsTables.changeDeliverTable();
                Instantiate(ParticlesManager.deliveredParticle, transform.position + Vector3.up, ParticlesManager.deliveredParticle.rotation);
            }
        }

        if(collision.gameObject.CompareTag(TagsLayers.enemyTag))
        {
            Interactor.handleInteraction(this, collision.gameObject.GetComponent<EnemyController>());
        }
    }

    void startDodge(EnemyController other)
    {
        Vibration.Vibrate(50);
        playerState = PLAYERSTATE.Dodged;

        float enemyPower = other.powerHandler.power;

        transform.forward = transform.getFaceDirection(other.transform);

        rb.AddForce(-transform.forward * (playerProps.dodgeForce + enemyPower), ForceMode.Impulse);

        if (currentDodgeRoutine != null)
            StopCoroutine(currentDodgeRoutine);

        DestroyBoostParticles();

        float dodgeTime = playerProps.dodgeTime;
        currentDodgeRoutine = StartCoroutine(returnDodgeBack(dodgeTime));
        anim.Play(PlayerAnimator.dodge_ClipName);
        anim.speed = PlayerAnimator.dodge_ClipLength / dodgeTime;
    }

    IEnumerator returnDodgeBack(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        playerState = PLAYERSTATE.Idle;
        anim.speed = defaultAnimSpeed;
    }

    void stealPlate(EnemyController other)
    {
        if (other == null || other.havePlate == false)
            return;

        List<Transform> stealablePlates = new List<Transform>();
        stealablePlates = other.getPlates(this);

        foreach (Transform t in stealablePlates)
            plates.Add(t);

        float scaleFactor = transform.lossyScale.y * transform.GetChild(0).lossyScale.y;
        plates.alignTransformPositions(plateStackDistance, Directions.Up, platesHolder, scaleFactor, Space.Self);    //From utilities class
        if(plates.Count >= minPlateForBoost)
            EnableSpeedBoost();

        Vibration.Vibrate(30);
    }

    void EnableSpeedBoost()
    {
        DestroyBoostParticles();
        Transform sp = ParticlesManager.stealPlateParticle;
        currentStealParticle = Instantiate(sp, transform.position + Vector3.up, sp.rotation);
        currentStealParticle.SetParent(transform);
        currentStealParticle.localScale = currentStealParticle.localScale * transform.localScale.y;

        currentBoostParticle = Instantiate(ParticlesManager.speedBoostParticle, transform.position + Vector3.up * .9f, ParticlesManager.speedBoostParticle.rotation);
        currentBoostParticle.SetParent(transform);
        currentBoostParticle.localScale = currentBoostParticle.localScale * transform.localScale.y;
        speedBoostMultiplier = ControlValues.speedBoostMultiplier;
        anim.speed = defaultAnimSpeed * speedBoostMultiplier;
        FunctionTimer.Create(() => DisableSpeedBooster(), ControlValues.speedBoostTime);
    }

    public void DisableSpeedBooster()
    {
        speedBoostMultiplier = 1;
        DestroyBoostParticles();
        if (playerState != PLAYERSTATE.Dodged)
            anim.speed = defaultAnimSpeed;
    }

    void DestroyBoostParticles()
    {
        if (currentStealParticle != null)
            Destroy(currentStealParticle.gameObject);
        if (currentBoostParticle != null)
            Destroy(currentBoostParticle.gameObject);
    }

    void CarryPlate(PlateSpawner ps)
    {
        //if (playerState == PLAYERSTATE.Dodged)
        //    return;

        if(currentDodgeRoutine != null)
        {
            StopCoroutine(currentDodgeRoutine);
            anim.speed = defaultAnimSpeed;
        }

        if (joyStick.minDisMoved(minInputDistance))
            playerState = PLAYERSTATE.CarryRun;
        else
            playerState = PLAYERSTATE.CarryIdle;

        foreach(Transform t in ps.getPlates())
        {
            plates.Add(t);
            t.SetParent(platesHolder);
            t.localPosition = Vector3.zero;
            t.localRotation = Quaternion.identity;
            t.localPosition = t.up * plateStackDistance / transform.GetChild(0).lossyScale.y / transform.lossyScale.y * (plates.Count - 1);
        }
        //Transform t = ps.getPlate();
        //plates.Add(t);
        //t.SetParent(platesHolder);
        //t.localPosition = Vector3.zero;
        //t.localRotation = Quaternion.identity;
        //t.localPosition = t.up * plateStackDistance / transform.GetChild(0).lossyScale.y * (plates.Count - 1);
    }

    public List<Transform> getPlates(EnemyController stealer)
    {
        startDodge(stealer);

        List<Transform> result = new List<Transform>();
        foreach (Transform t in plates)
            result.Add(t);

        plates.Clear();
        havePlates = false;

        return result;
    }

    public void GetInteractiosStatus(Interaction inter, EnemyController other)
    {
        switch(inter)
        {
            case Interaction.Dodge:
                startDodge(other);
                break;

            case Interaction.Steal:
                stealPlate(other);
                break;

            default:
                break;
        }
    }
}

public enum PLAYERSTATE
{
    Idle,
    Run,
    CarryIdle,
    CarryRun,
    Dodged
}

public enum InteractionStatus
{
    I,
    You,
    None,
    Both
}
