﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    public static LevelController instance;

    int currentLevel = 0;
    public int TotalPlates { get; private set; }

    public const int TotalLevels = 5;
    [SerializeField] int[] platesForLevels = null;

    public int[] PlatesForEachLevel => platesForLevels;

    private void Awake()
    {
        currentLevel = DataManager.LoadLevel();
        TotalPlates = platesForLevels[currentLevel-1];
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.LoadScene(currentLevel, LoadSceneMode.Additive);

        #region Singleton
        if (instance != null)
            Destroy(instance);

        instance = this;
        #endregion
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode loadMode)
    {
        if(scene.buildIndex == currentLevel)
            PlayersManager.instance.OnSceneLoaded();
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
