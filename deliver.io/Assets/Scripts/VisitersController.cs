﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class VisitersController : MonoBehaviour
{
    Animator anim = null;

    [SerializeField] float minChangeTime = 3f;
    [SerializeField] float maxChangeTime = 8f;
    float currentChangeTime = 5;

    const string index_Param = "Index";
    const string name_Param = "Sitting_";

    int MaxClips = 0;
    int currentIndex = 1;

    private void Start()
    {
        anim = GetComponent<Animator>();
        anim.speed = 2;

        MaxClips = anim.runtimeAnimatorController.animationClips.Length;

        anim.Play(name_Param + Random.Range(1, MaxClips));
        resetAnimation();
    }

    private void Update()
    {
        currentChangeTime -= Time.deltaTime;
        if(currentChangeTime <= 0)
        {
            resetAnimation();
        }
    }

    void resetAnimation()
    {
        currentChangeTime = Random.Range(minChangeTime, maxChangeTime);

        int r = Random.Range(1,MaxClips);

        currentIndex = r;
        anim.SetInteger(index_Param, currentIndex);
    }
}
