﻿using UnityEngine;

public class PlateSpawner : MonoBehaviour
{
    const int minPlate = 1;
    const int maxPlate = 4;

    Transform spawnPoint;

    Transform platePrefab = null;
    Transform[] foodGroups = null;

    [SerializeField] float spawnDuration = 1f;
    float tolerance = 1.5f;
    float currentSpawnDuration = 0;

    public Transform currentPlate { get; private set; }
    [SerializeField] Transform[] currentPlates = null;
    public bool havePlate { get; private set; }
    float temp = 0;

    bool psSpawned = false;
    Transform havePlateParticle = null;

    void Start()
    {
        spawnPoint = transform.Find("SpawnPoint");

        platePrefab = PrefabManager.platePrefab;
        foodGroups = new Transform[PrefabManager.foodGroup.Length];
        foodGroups = PrefabManager.foodGroup;

        currentSpawnDuration = getCurrentSpawnDuration(tolerance);
        temp = 0;
        havePlate = false;
    }

    private void Update()
    {
        if (GameManager.gameState != GameState.InGame)
            return;

        //if (currentPlate == null)
        //    havePlate = false;

        if (currentPlates == null)
            havePlate = false;

        if(havePlate && havePlateParticle == null)
        {
            SpawnWhilePlateParticle();
        }
        else if(havePlate == false && havePlateParticle != null)
        {
            Destroy(havePlateParticle.gameObject);
            havePlateParticle = null;
        }

        if(havePlate == false)
        {
            temp += Time.deltaTime;

            if(psSpawned == false && temp >= currentSpawnDuration -0.25f)
            {
                Transform t = Instantiate(ParticlesManager.plateSpawnParticle, spawnPoint.position, ParticlesManager.plateSpawnParticle.rotation);
                t.localScale = Vector3.one * 0.9f;
                psSpawned = true;
            }

            if(temp >= currentSpawnDuration)
            {
                spawnPlates();
            }
        }
    }

    //Spawn only one plate on the table
    void spawnPlate()
    {
        currentPlate = Instantiate(platePrefab, spawnPoint.position, platePrefab.rotation);
        int r = Random.Range(0, foodGroups.Length);
        Transform foods = Instantiate(foodGroups[r], currentPlate.position, currentPlate.rotation);
        foods.SetParent(currentPlate);
        foods.localRotation = Quaternion.Euler(new Vector3(foods.localRotation.x, Random.Range(0, 360), foods.localRotation.z));

        havePlate = true;
        temp = 0;
        currentSpawnDuration = getCurrentSpawnDuration(tolerance);
        psSpawned = false;
    }

    void spawnPlates()
    {
        int num = Random.Range(minPlate, maxPlate);
        currentPlates = new Transform[num];
        float yPos = 0;

        for(int i = 0; i < currentPlates.Length; i++)
        {
            Transform t = Instantiate(platePrefab, spawnPoint.position + Vector3.up * yPos, platePrefab.rotation);
            int r = Random.Range(0, foodGroups.Length);
            Transform foods = Instantiate(foodGroups[r], t.position + Vector3.up, t.rotation);
            foods.SetParent(t);
            foods.localPosition = Vector3.zero;
            foods.localRotation = Quaternion.Euler(new Vector3(foods.localRotation.x, Random.Range(0, 360), foods.localRotation.z));
            currentPlates[i] = t;

            yPos += ControlValues.plateStackDistance;
        }

        havePlate = true;
        temp = 0;
        currentSpawnDuration = getCurrentSpawnDuration(tolerance);
        psSpawned = false;
    }

    void SpawnWhilePlateParticle()
    {
        havePlateParticle = Instantiate(ParticlesManager.whilePlateParticle, spawnPoint.position, ParticlesManager.whilePlateParticle.rotation);
    }

    float getCurrentSpawnDuration(float variance)
    {
        float min = spawnDuration;
        float max = spawnDuration + variance;

        return Random.Range(min, max);
    }

    public Transform getPlate()
    {
        if (!havePlate)
            return null;

        havePlate = false;
        Transform p = currentPlate;
        currentPlate = null;

        return p;
    }

    public Transform[] getPlates()
    {
        if (!havePlate)
            return null;

        havePlate = false;
        Transform[] result = currentPlates;
        currentPlates = null;
        return result;
    }
}
