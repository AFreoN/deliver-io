﻿using UnityEngine;
using UnityEngine.SceneManagement;
using GameAnalyticsSDK;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public static GameState gameState { get; private set; }

    private void Awake()
    {
        Application.targetFrameRate = 60;
        instance = this;

        gameState = GameState.Menu;
        GameAnalytics.Initialize();
        //Time.timeScale = 1.3f;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            UIManager.instance.OnBack();
        }
    }

    public void StartGame()
    {
        if(DataManager.currentLevel == 1 && DataManager.isTutorialNeeded)
        {
            UIManager.instance.EnableTutorial();
            return;
        }
        UIManager.instance.GameStarted();
        gameState = GameState.InGame;

        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Level_" + DataManager.currentLevel, 0);
    }

    public void ForceStartGame()
    {
        DataManager.disableTutorial();
        UIManager.instance.DisableTutorial();
        gameState = GameState.InGame;
    }

    public void GameWon()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Level_" + DataManager.currentLevel, AdRewards.currentCoins);

        PlayersManager.instance.DisablePlayerCanvas();
        UIManager.instance.GameWon();
        DataManager.SaveLevel();
        gameState = GameState.Win;

        CameraController.current.GoDefault();
    }

    public void GameLose()
    {
        UIManager.instance.GameLose();
        gameState = GameState.Lose;

        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "Level_" + DataManager.currentLevel, AdRewards.currentCoins);
        Advert_Manager.instance.ShowInterstialAd();
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //public void ReloadOnWin()
    //{
    //    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    //    Advert_Manager.instance.ShowInterstialAd();
    //}
}

public enum GameState
{
    Menu,
    InGame,
    Lose,
    Win
}
