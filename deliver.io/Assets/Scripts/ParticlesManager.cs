﻿using UnityEngine;

public class ParticlesManager : MonoBehaviour
{
    [SerializeField] Transform PlayerHitParticle = null;
    [SerializeField] Transform PlateSpawnParticle = null;
    [SerializeField] Transform WhilePlateParticle = null;

    [SerializeField] Transform TableSmileyParticle = null;
    [SerializeField] Transform StealPlateParticle = null;
    [SerializeField] Transform SpeedBoostParticle = null;

    [SerializeField] Transform DeliveredParticle = null;
    [SerializeField] Transform SizeUpParticle = null;

    public static Transform playerHitParticle { get; private set; }
    public static Transform plateSpawnParticle { get; private set; }
    public static Transform whilePlateParticle { get; private set; }
    public static Transform tableSmileyParticle { get; private set; }
    public static Transform stealPlateParticle { get; private set; }
    public static Transform speedBoostParticle { get; private set; }
    public static Transform deliveredParticle { get; private set; }
    public static Transform sizeUpParticle { get; private set; }

    private void Awake()
    {
        playerHitParticle = PlayerHitParticle;
        plateSpawnParticle = PlateSpawnParticle;
        whilePlateParticle = WhilePlateParticle;
        tableSmileyParticle = TableSmileyParticle;
        stealPlateParticle = StealPlateParticle;
        speedBoostParticle = SpeedBoostParticle;
        deliveredParticle = DeliveredParticle;
        sizeUpParticle = SizeUpParticle;
    }
}
