﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class PlayersManager : MonoBehaviour
{
    public static PlayersManager instance;

    [SerializeField] Transform crownPrefab = null;
    const string crownName = "Crown";

    public static List<Transform> allPlayers { get; private set; } //= new List<Transform>();
    List<PowerHandler> allPowerhandlers = new List<PowerHandler>();
    List<Crown> allCrowns = new List<Crown>();

    public List<string> Names = new List<string>();

    public static int currentHighScore { get; private set; }

    private void Awake()
    {
        instance = this;
        allPlayers = new List<Transform>();
        currentHighScore = 0;
    }

    public void OnSceneLoaded()
    {
        Transform player = GameObject.FindGameObjectWithTag(TagsLayers.playerTag).transform;

        GameObject[] enemies = GameObject.FindGameObjectsWithTag(TagsLayers.enemyTag);
        assignNames(enemies);

        allPlayers.Add(player);
        foreach (GameObject g in enemies)
        {
            allPlayers.Add(g.transform);
        }

        AfterLoad();
    }

    void AfterLoad()
    {
        allPowerhandlers.Add(allPlayers[0].GetComponent<PlayerController>().powerHandler);
        allCrowns.Add(allPlayers[0].GetComponent<Crown>());

        for (int i = 1; i < allPlayers.Count; i++)
        {
            allPowerhandlers.Add(allPlayers[i].GetComponent<EnemyController>().powerHandler);
            allCrowns.Add(allPlayers[i].GetComponent<Crown>());
        }
    }

    public void SetHighScore(int score)
    {
        if (score >= currentHighScore)
        {
            currentHighScore = score;
            assignCrowns();
        }

        if(score >= LevelController.instance.TotalPlates && allPowerhandlers[0].platesDelivered != currentHighScore)
        {
            GameManager.instance.GameLose();
            DisablePlayerCanvas();

            List<PowerHandler> sorted = SortPowerHandler();
            string[] s = new string[sorted.Count];
            int playerID = 0;
            for (int i = 0; i < s.Length; i++)
            {
                s[i] = sorted[i].thisPlayer.name;
                if (sorted[i].thisPlayer.CompareTag(TagsLayers.playerTag))
                    playerID = i;
            }

            Ranking.instance.ShowPlayerName(s,playerID);

            //List<int> nums = new List<int>();
            //foreach(PowerHandler p in allPowerhandlers)
            //{
            //    nums.Add(p.platesDelivered);
            //}
            //nums.Sort();
            //nums.Reverse();
            //foreach (int i in nums)
            //    Debug.Log("Nums = " + i);

            //string[] s = new string[nums.Count];
            //for(int i = 0; i < nums.Count; i++)
            //{
            //    int index = nums.IndexOf(allPowerhandlers[i].platesDelivered);
            //    s[i] = allPowerhandlers[index].thisPlayer.name;
            //    Debug.Log(allPowerhandlers[index].thisPlayer.name + "  = " + nums[i]);
            //}
            //Ranking.instance.ShowPlayerName(s);
        }
    }

    List<PowerHandler> SortPowerHandler()
    {
        return allPowerhandlers.OrderBy(x => -x.platesDelivered).ToList();
    }

    public void DisablePlayerCanvas()
    {
        foreach (Transform t in allPlayers)
        {
            t.GetComponent<PlayerCanvas>().canvas.gameObject.SetActive(false);
        }
    }

    void assignCrowns()
    {
        for (int i = 0; i < allPlayers.Count; i++)
        {
            Transform player = allPlayers[i];

            if (allPowerhandlers[i].platesDelivered >= currentHighScore)
            {
                //Assign crown here
                allCrowns[i].assignCrown(crownPrefab);
            }
            else
            {
                //Remove crown here
                allCrowns[i].removeCrown();
            }
        }
    }

    void assignNames(GameObject[] enemies)
    {
        if (Names.Count < enemies.Length)
        {
            Debug.LogWarning("Not Enough Names");
            return;
        }

        for (int i = 0; i < enemies.Length; i++)
        {
            int r = Random.Range(0, Names.Count);
            enemies[i].name = Names[r];
            enemies[i].GetComponent<PlayerCanvas>().nameText.text = Names[r];
            Names.RemoveAt(r);
        }
    }
}
