﻿using UnityEngine;

public class Chef : MonoBehaviour
{
    Animator anim = null;
    int totalClips = 0;
    const string paraName = "move";

    float tempTime = 0;
    float intervalToSwitch = 3;
    float variance = 2;

    float minAnimSpeed = 0.85f, maxAnimSpeed = 1.15f;

    private void Start()
    {
        anim = GetComponent<Animator>();

        totalClips = anim.runtimeAnimatorController.animationClips.Length + 1;

        int r = Random.Range(1, totalClips);
        anim.SetInteger(paraName, r);
        anim.speed = Random.Range(minAnimSpeed, maxAnimSpeed);

        tempTime = intervalToSwitch + Random.Range(0, variance);
    }

    private void Update()
    {
        tempTime -= Time.deltaTime;
        if(tempTime <= 0)
        {
            resetAnimation();
        }
    }

    void resetAnimation()
    {
        int r = Random.Range(1, totalClips);
        anim.SetInteger(paraName, r);

        tempTime = intervalToSwitch + Random.Range(0, variance);
        anim.speed = Random.Range(minAnimSpeed, maxAnimSpeed);
    }
}
