﻿using UnityEngine;
using System.Collections.Generic;

public class ESkin : MonoBehaviour
{
    [SerializeField] int Head = 0;
    [SerializeField] int Upper = 8;
    [SerializeField] int Lower = 8;

    SkinnedMeshRenderer currentCloth = null;   //Skin to change
    [SerializeField] Transform hip = null;

    [SerializeField] SkinnedMeshRenderer body = null;      //Reference skin for bones

    private void Start()
    {
        currentCloth = AssignSkin.instance.headGears[Head];
        if (currentCloth != null)
            ChangeSkin(currentCloth, body, hip);

        currentCloth = AssignSkin.instance.upperGears[Upper];
        if (currentCloth != null)
            ChangeSkin(currentCloth, body, hip);

        currentCloth = AssignSkin.instance.lowerGears[Lower];
        if (currentCloth != null)
            ChangeSkin(currentCloth, body, hip);
    }

    void ChangeSkin(SkinnedMeshRenderer thisSkin, SkinnedMeshRenderer targetSkin, Transform rootBone)
    {
        GameObject g = Instantiate(thisSkin.gameObject, thisSkin.transform.position, thisSkin.transform.rotation);
        thisSkin = g.GetComponent<SkinnedMeshRenderer>();

        Dictionary<string, Transform> boneMap = new Dictionary<string, Transform>();
        foreach (Transform bone in targetSkin.bones)
        {
            boneMap[bone.name] = bone;
        }

        Transform[] boneArray = thisSkin.bones;

        for (int i = 0; i < boneArray.Length; i++)
        {
            string boneName = boneArray[i].name;
            if (false == boneMap.TryGetValue(boneName, out boneArray[i]))
            {
                Debug.LogError("failed to get bone: " + boneName);
                Debug.Break();
            }
        }
        thisSkin.bones = boneArray;
        thisSkin.rootBone = rootBone;
    }
}
