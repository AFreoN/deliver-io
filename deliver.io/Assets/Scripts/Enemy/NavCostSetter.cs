﻿using UnityEngine;
using UnityEngine.AI;

public class NavCostSetter : MonoBehaviour
{
    public static NavCostSetter instance;

    const int layer1 = 3;
    const int layer2 = 4;

    [SerializeField] float minTime = 3f;
    [SerializeField] float maxTime = 6f;

    [SerializeField] float minAreaCost = 1;
    [SerializeField] float maxAreaCost = 4;

    float tempTime = 0;

    private void Awake()
    {
        if (instance != null)
            Destroy(instance);

        instance = this;
    }

    private void Start()
    {
        resetCostAndTime();
    }

    private void Update()
    {
        tempTime -= Time.deltaTime;
        if(tempTime <= 0)
        {
            resetCostAndTime();
        }
    }

    void resetCostAndTime()
    {
        tempTime = Random.Range(minTime, maxTime);

        float mid = (minAreaCost + maxAreaCost) * .5f;
        float r1 = Random.Range(minAreaCost, mid);
        float r2 = Random.Range(mid, maxAreaCost);

        bool one = Utilities.roll(2);
        NavMesh.SetAreaCost(layer1, one == true ? r1 : r2);
        NavMesh.SetAreaCost(layer2, one == false ? r1 : r2);
    }
}
