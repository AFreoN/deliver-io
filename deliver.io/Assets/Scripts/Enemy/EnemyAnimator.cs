﻿using UnityEngine;

public class EnemyAnimator : MonoBehaviour
{
    Animator anim;
    EnemyController ec;

    [SerializeField] RuntimeAnimatorController danceController = null;
    PowerHandler powerHandler = null;
    bool finished = false;

    const string run_Param = "run";
    const string carry_Param = "carry";
    public const string dodge_ClipName = "Dodging Back";
    public static float dodge_ClipLength { get; private set; }

    private void Start()
    {
        anim = GetComponent<Animator>();
        ec = GetComponent<EnemyController>();
        powerHandler = GetComponent<EnemyController>().powerHandler;

        if (dodge_ClipLength > Mathf.Epsilon)
            return;

        foreach (AnimationClip clip in anim.runtimeAnimatorController.animationClips)
        {
            if (clip.name == dodge_ClipName)
            {
                dodge_ClipLength = clip.length;
            }
        }
    }

    private void Update()
    {
        if (GameManager.gameState == GameState.Lose && PlayersManager.currentHighScore == powerHandler.platesDelivered)
        {
            anim.speed = 1;
            if (finished)
                return;
            else
            {
                anim.runtimeAnimatorController = danceController;
                int r = Random.Range(1, 4);
                anim.SetInteger("move", r);
                transform.forward = Vector3.back;
                finished = true;
            }
        }

        //if (ec.enemyState == ENEMYSTATE.Run || ec.enemyState == ENEMYSTATE.CarryRun)
        //    anim.speed = 1.3f;
        //else if (ec.enemyState != ENEMYSTATE.Dodged)
        //    anim.speed = 1;

        switch (ec.enemyState)
        {
            case ENEMYSTATE.Idle:
                SetAnimValues(false, false);
                break;

            case ENEMYSTATE.Run:
                SetAnimValues(true, false);
                break;

            case ENEMYSTATE.CarryIdle:
                SetAnimValues(false, true);
                break;

            case ENEMYSTATE.CarryRun:
                SetAnimValues(true, true);
                break;

            case ENEMYSTATE.Dodged:
                SetAnimValues(false, false);
                break;
        }
    }

    void SetAnimValues(bool run, bool carry)
    {
        resetAnimation(run_Param, run);
        resetAnimation(carry_Param, carry);
    }

    void resetAnimation(string s, bool isTrue = false)
    {
        if (finished)
            return;

        if (anim.GetBool(s) != isTrue)
            anim.SetBool(s, isTrue);
    }
}
