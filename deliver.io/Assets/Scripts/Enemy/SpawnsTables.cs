﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnsTables : MonoBehaviour
{
    public static SpawnsTables instance { get; private set; }

    public static PlateSpawner[] spawnPoints { get; private set; }
    public static Table[] tables { get; private set; }

    public static Table currentDeliverTable;
    static int totalTableCount, currentTableIndex;

    void Awake()
    {
        instance = this;

        //Initialize();
        //currentDeliverTable = tables[currentTableIndex].getTable();

        SceneManager.sceneLoaded += setTable;
    }
    public void setTable(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex == DataManager.currentLevel)
        {
            Initialize();
        }
    }

    public void Initialize()
    {
        Component[] comp = GameObject.FindGameObjectsWithTag(TagsLayers.spawnPointTag).ToCustomArray(typeof(PlateSpawner));
        spawnPoints = System.Array.ConvertAll(comp, item => item as PlateSpawner);

        Component[] tableComp = GameObject.FindGameObjectsWithTag(TagsLayers.tableTag).ToCustomArray(typeof(Table));
        tables = System.Array.ConvertAll(tableComp, item => item as Table);
        foreach (Table t in tables)
            t.notThisTable();

        totalTableCount = tables.Length;
        currentTableIndex = 0;

        currentDeliverTable = tables[currentTableIndex].getTable();
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= setTable;
    }
    public static void changeDeliverTable()
    {
        resetAllTables();

        if (Utilities.roll(3))
        {
            Vector3 localPos = new Vector3(0.923f, 1.113f, 1.12f);
            Transform t = Instantiate(ParticlesManager.tableSmileyParticle, currentDeliverTable.transform.position, ParticlesManager.tableSmileyParticle.rotation);
            t.position += localPos;
        }

        currentTableIndex++;
        currentTableIndex = currentTableIndex % totalTableCount;

        currentDeliverTable = tables[currentTableIndex].getTable();
    }

    static void resetAllTables()
    {
        foreach (Table t in tables)
            t.notThisTable();
    }
}
