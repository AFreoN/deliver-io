﻿using UnityEngine;

public class PassiveEnemyControl : MonoBehaviour
{
    Transform player = null;
    Vector3 initPosition = Vector3.zero;

    private void Start()
    {
        player = transform.parent;
        initPosition = transform.localPosition;
    }

    private void Update()
    {
        transform.localPosition = initPosition;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag(TagsLayers.enemyTag) && other.transform != player)
        {
            //Debug.Log(player.name + " collided " + other.gameObject.name);
            Interactor.handleInteraction(player.GetComponent<EnemyController>(), other.gameObject.GetComponent<EnemyController>());
        }

        if(other.gameObject.CompareTag(TagsLayers.playerTag))
        {
            //Debug.Log("Triggered Player");
        }
    }
}
