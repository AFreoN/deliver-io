﻿using UnityEngine;

public class JamBreaker : MonoBehaviour
{
    public Transform player1 = null;
    public Transform player2 = null;

    float initDifference = 0;
    float disVariation = 1f;

    float tempTime = 10;

    private void Start()
    {
        tempTime = ControlValues.interactionDelay;
    }

    public void SetValues(Transform t1, Transform t2)
    {
        player1 = t1;
        player2 = t2;
        initDifference = Vector3.Distance(t1.position, t2.position);
        tempTime = ControlValues.interactionDelay + .02f;

        Debug.Log("JB Created");
    }

    private void Update()
    {
        //Check if they move far away
        if(Vector3.Distance(player1.position, player2.position) > initDifference + disVariation)
        {
            Debug.Log("Moved Far Away");
            Destroy(gameObject);
        }

        tempTime -= Time.deltaTime;
        if(tempTime <= 0)
        {
            Debug.Log("Interactor used");

            EnemyController ec1 = player1.GetComponent<EnemyController>();
            EnemyController ec2 = player2.GetComponent<EnemyController>();

            Interactor.handleInteraction(ec1, ec2);
            Destroy(gameObject);
        }
    }
}
