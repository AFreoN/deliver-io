﻿using UnityEngine;

public class WinParticles : MonoBehaviour
{
    [SerializeField] Transform confettiPrefab = null;
    [SerializeField] Transform starsPrefab = null;

    bool effectStarted = false;
    Transform player = null;

    float sideDis = 2;
    float spawnRadius = 3;

    //timer
    float interval = 1;
    float tempTime = 0;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag(TagsLayers.playerTag).transform;

        tempTime = interval;
    }

    private void Update()
    {
        if (GameManager.gameState == GameState.Win)
        {
            if (effectStarted == false)
                StartEffects();
        }
        else
            return;

        if (effectStarted)
        {
            tempTime -= Time.deltaTime;
            if (tempTime <= 0)
            {
                spawnStars();
            }
        }
    }

    public void StartEffects()
    {
        if (player != null)
        {
            Transform confet1 = Instantiate(confettiPrefab, player.position + Vector3.right * sideDis + Vector3.up * Random.Range(.1f,.5f), confettiPrefab.rotation);
            Transform confet2 = Instantiate(confettiPrefab, player.position + Vector3.left * sideDis + Vector3.up * Random.Range(.1f, .5f), confettiPrefab.rotation);
        }

        effectStarted = true;
    }

    public void spawnStars()
    {

        Vector3 starPos = player.position + Vector3.up * spawnRadius * 0.5f + Random.insideUnitSphere * spawnRadius;
        Transform star = Instantiate(starsPrefab, starPos, starsPrefab.rotation);
        tempTime = interval + Random.Range(0, 1);
    }
}