﻿using UnityEngine;

public class Crown : MonoBehaviour
{
    public Transform head = null;
    float placeHeight = .45f;
    Transform currentCrown = null;

    float startScale = 0;
    Transform hip = null;

    private void Start()
    {
        hip = transform.GetChild(0);
    }

    public void assignCrown(Transform crownPrefab)
    {
        startScale = crownPrefab.localScale.y;

        if(currentCrown == null)
        {
            currentCrown = Instantiate(crownPrefab);
            currentCrown.SetParent(head);
            assignPositions();
        }
        else
        {
            assignPositions();
        }
    }

    void assignPositions()
    {
        currentCrown.localPosition = Vector3.zero + Vector3.up * placeHeight / hip.localScale.y;
        currentCrown.localScale = Vector3.one * startScale * (1 + (transform.localScale.y-1) * 0.25f) / hip.localScale.y;
        currentCrown.localRotation = Quaternion.identity;
    }

    public void removeCrown()
    {
        if(currentCrown != null)
        {
            Destroy(currentCrown.gameObject);
        }
    }
}
