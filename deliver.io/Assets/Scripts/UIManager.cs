﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    [Header("Play Btn")] [SerializeField] Button playBtn = null;

    [Header("Panels")]
    public GameObject mainMenuPanel;
    public GameObject InGamePanel;
    public GameObject WonPanel;
    public GameObject LosePanel;
    public GameObject tutorialPanel;
    public GameObject settingsPanel;

    [Header("Shop")]
    public GameObject shopPanel;
    [SerializeField] GameObject shopPlayer = null;

    [Header("Level Texts")]
    [SerializeField] Text levelTextMain = null;
    [SerializeField] Text levelTextWon = null;

    [Header("Coins & names")]
    [SerializeField] InputField playerNameField = null;
    [SerializeField] Text mainMenuCoinText = null;

    [SerializeField] GameObject shopMarkerImg = null;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        startSetter();
    }

    void startSetter()
    {
        playBtn.interactable = false;
        FunctionTimer.Create(() => EnablePlayBtn(), .5f);

        mainMenuPanel.SetActive(true);
        InGamePanel.SetActive(false);
        WonPanel.SetActive(false);
        LosePanel.SetActive(false);
        tutorialPanel.SetActive(false);
        settingsPanel.SetActive(false);

        shopPanel.SetActive(false);
        shopPlayer.SetActive(false);
        shopPlayer.GetComponent<AssignSkin>().AssignToPlayer();

        levelTextMain.text = "LEVEL  <color=#FFFF00>" + DataManager.currentLevel + "</color>";
        levelTextWon.text = "LEVEL   <color=#FFFF00>" + DataManager.currentLevel + "  >>  " + (DataManager.currentLevel +1) + "</color>";

        playerNameField.text = DataManager.LoadPlayerName;
        mainMenuCoinText.text = DataManager.Coins.ToString();

        IsSkinAvailable();
    }

    public void IsSkinAvailable()
    {
        int coins = DataManager.Coins;
        if (coins >= ShopPanel.HeadCost)
        {
            bool b = false;
            foreach (int i in DataManager.loadHeadSkinData())
            {
                if (i == 0)
                {
                    b = true;
                    break;
                }
            }
            shopMarkerImg.SetActive(b);
        }
        else if (coins >= ShopPanel.UpperCost)
        {
            bool b = false;
            foreach (int i in DataManager.loadUpperSkinData())
            {
                if (i == 0)
                {
                    b = true;
                    break;
                }
            }
            shopMarkerImg.SetActive(b);
        }
        else if (coins >= ShopPanel.LowerCost)
        {
            bool b = false;
            foreach (int i in DataManager.loadLowerSkinData())
            {
                if (i == 0)
                {
                    b = true;
                    break;
                }
            }
            shopMarkerImg.SetActive(b);
        }
        else
            shopMarkerImg.SetActive(false);
    }
    
    public void EnablePlayBtn()
    {
        playBtn.interactable = true;
    }

    public void GameStarted()
    {
        mainMenuPanel.SetActive(false);
        InGamePanel.SetActive(true);
    }

    public void EnableTutorial()
    {
        mainMenuPanel.SetActive(false);
        tutorialPanel.SetActive(true);
    }

    public void DisableTutorial()
    {
        tutorialPanel.SetActive(false);
        InGamePanel.SetActive(true);
    }

    //public void disableDragtoMoveText()
    //{
    //    dragToMoveText.SetActive(false);
    //}

    public void GameWon()
    {
        InGamePanel.SetActive(false);
        WonPanel.SetActive(true);
    }

    public void GameLose()
    {
        InGamePanel.SetActive(false);
        LosePanel.SetActive(true);
    }

    public void OpenSkinShop()
    {
        shopPanel.SetActive(true);
        shopPlayer.SetActive(true);
        mainMenuPanel.SetActive(false);
    }

    public void CloseSkinShop()
    {
        shopPlayer.GetComponent<AssignSkin>().AssignToPlayer();
        mainMenuPanel.SetActive(true);
        shopPanel.SetActive(false);
        shopPanel.SetActive(false);

        mainMenuCoinText.text = DataManager.Coins.ToString();
        IsSkinAvailable();
    }

    public void OnBack()
    {
        if (shopPanel.activeInHierarchy)
        {
            CloseSkinShop();
        }
        else if(settingsPanel.activeInHierarchy)
        {
            CloseSettings();
        }
        else if (GameManager.gameState != GameState.Menu)
            GameManager.instance.ReloadScene();
        else
            Application.Quit();
    }

    public void OpenSettings()
    {
        settingsPanel.SetActive(true);
    }

    public void CloseSettings()
    {
        settingsPanel.SetActive(false);
    }
}
