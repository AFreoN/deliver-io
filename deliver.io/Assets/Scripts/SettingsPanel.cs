﻿using UnityEngine;

public class SettingsPanel : MonoBehaviour
{
    [Header("Tick Images")]
    [SerializeField] Transform vibrationTick = null;
    [SerializeField] Transform joystickTick = null;

    [Header("Button transforms")]
    [SerializeField] Transform vibrationOn = null;
    [SerializeField] Transform vibrationOff = null;
    [SerializeField] Transform joystickDynamic = null;
    [SerializeField] Transform joystickStatic = null;

    Vector3 vLocalPos = Vector3.zero;
    Vector3 jLocalPos = Vector3.zero;

    private void OnEnable()
    {
        vLocalPos = vibrationTick.localPosition;
        jLocalPos = joystickTick.localPosition;
        UpdateUI();
    }

    void UpdateUI()
    {
        if (DataManager.vibration == 1)
            vibrationTick.SetParent(vibrationOn);
        else
            vibrationTick.SetParent(vibrationOff);

        if (DataManager.dynamicJoystick == 1)
            joystickTick.SetParent(joystickDynamic);
        else
            joystickTick.SetParent(joystickStatic);

        vibrationTick.localScale = Vector3.one;
        vibrationTick.localPosition = vLocalPos;

        joystickTick.localScale = Vector3.one;
        joystickTick.localPosition = jLocalPos;
    }

    public void ToggleVibration(int on)
    {
        DataManager.ToggleVibration(on);
        UpdateUI();
    }

    public void ToggleJoystick(int on)
    {
        DataManager.ToggleJoystick(on);
        UpdateUI();
    }
}
