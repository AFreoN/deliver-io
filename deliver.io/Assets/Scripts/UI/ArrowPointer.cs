﻿using UnityEngine;
using UnityEngine.UI;

public class ArrowPointer : MonoBehaviour
{
    [SerializeField] RectTransform dirIndicator = null;

    Transform target = null;
    Camera cam = null;

    Transform player = null;
    PlayerController playerController = null;
    Vector3 spawnPosition = Vector3.zero;

    [SerializeField] float border = 100f;
    float width = 0, height = 0;

    void Start()
    {
        cam = Camera.main;
        player = GameObject.FindGameObjectWithTag(TagsLayers.playerTag).transform;
        playerController = player.GetComponent<PlayerController>();

        width = Screen.width;
        height = Screen.height;
    }

    void LateUpdate()
    {
        if (GameManager.gameState == GameState.Win || GameManager.gameState == GameState.Lose)
        {
            Destroy(dirIndicator.gameObject);
            Destroy(gameObject);
        }

        if(playerController.havePlates)
        {
            target = SpawnsTables.currentDeliverTable.transform.GetChild(1);
            if (target == null)
                return;

            //float ang = Utilities.getAngleFromVectorsXZ(target.position, player.position, -90);
            //dirIndicator.transform.rotation = Quaternion.Euler(90, 0, ang);

            Vector2 screenPos = cam.WorldToScreenPoint(target.position);
            bool right = true;

            //screenPos = cam.transform.position.z >= target.position.z ? screenPos * -1 : screenPos;
            if(Vector3.Dot(cam.transform.forward, (target.position - cam.transform.position).normalized) <= 0)
            {
                screenPos *= -1;
                right = false;
            }

            if (screenPos.x < border)
                screenPos.x = border;
            else if (screenPos.x > width - border)
                screenPos.x = width - border;

            if (screenPos.y < border)
                screenPos.y = border;
            else if (screenPos.y > height - border)
                screenPos.y = height - border;

            dirIndicator.position = Vector2.Lerp(dirIndicator.position, screenPos,.6f);
            if(right)
                dirIndicator.right = (target.ToScreenPos(cam) - player.ToScreenPos(cam)).normalized;
            else
                dirIndicator.right = (target.ToScreenPos(cam) - player.ToScreenPos(cam)).normalized * -1;
        }
        else
        {
            dirIndicator.position = Vector3.one * -5000;
            //Vector3 platePos = getRandomSpawnPosition();

            //if (platePos != Vector3.zero)
            //{
            //    //float ang = Utilities.getAngleFromVectorsXZ(platePos, player.position, -90);
            //    //dirIndicator.transform.rotation = Quaternion.Euler(90, 0, ang);
            //    Vector2 screenPos = cam.WorldToScreenPoint(platePos);
            //    dirIndicator.position = screenPos;
            //    dirIndicator.right = (platePos.ToScreenPos(cam) - player.position.ToScreenPos(cam)).normalized;
            //}
            //else
            //{
            //    //dirIndicator.transform.localRotation = Quaternion.Euler(Vector3.right * 90);
            //    dirIndicator.position = Vector2.one * -5000;
            //}
        }
    }

    Vector3 getRandomSpawnPosition()
    {
        float dis = float.MaxValue;
        Vector3 result = Vector3.zero;

        foreach(PlateSpawner ps in SpawnsTables.spawnPoints)
        {
            if (ps.havePlate == false)
                continue;
            float curDis = Vector3.Distance(player.position, ps.transform.position);
            if(curDis < dis)
            {
                dis = curDis;
                result = ps.transform.position;
            }
        }

        return result;
    }
}
