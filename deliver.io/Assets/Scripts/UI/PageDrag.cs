﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;

public class PageDrag : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    Vector2 panelPosition = Vector2.zero;

    const int pageSize = 900;
    const int minDistance = -2250;
    const int maxDistance = -450;

    const int headUIPosition = -450;
    const int upperUIPosition = -1350;
    const int lowerUIPosition = -2250;

    int currentLocalpos = -450;
    //bool onDrag = false;
    Coroutine currentRoutine = null;

    [SerializeField][Range(0.05f,1)] float percentThresold = .2f;
    [SerializeField] float easing = .5f;

    [Header("Arrows")]
    [SerializeField] GameObject headArrow = null;
    [SerializeField] GameObject upperArrow = null;
    [SerializeField] GameObject lowerArrow = null;

    [Header("Points")]
    [SerializeField] GameObject headPoint = null;
    [SerializeField] GameObject upperPoint = null;
    [SerializeField] GameObject lowerPoint = null;

    void Start()
    {
        panelPosition = transform.position;
        currentLocalpos = -450;
        SetUIElements();
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(2))
        {
            Debug.Log("pos = " + transform.localPosition);
        }

        //if(onDrag == false && Mathf.Abs( transform.localPosition.y) > 0)
        //{
        //    if (currentRoutine != null)
        //        StopCoroutine(currentRoutine);

        //    transform.localPosition = Vector2.Lerp(transform.localPosition, new Vector2(currentLocalpos, 0), .2f);
        //}
    }

    public void OnBeginDrag(PointerEventData data)
    {
        //onDrag = true;
        panelPosition = transform.position;
    }

    public void OnDrag(PointerEventData data)
    {
        float difference = data.pressPosition.x - data.position.x;
        transform.position = panelPosition - new Vector2(difference, 0);
    }

    public void OnEndDrag(PointerEventData data)
    {
        float difference = (data.pressPosition.x - data.position.x) / Screen.width;
        if (Mathf.Abs(difference) >= percentThresold)   //Required distance is moved
        {
            if(currentRoutine != null)
                StopCoroutine(currentRoutine);

            int mul = 1;
            if(difference > 0)
            {
                mul = -1;
            }

            currentLocalpos += pageSize * mul;
            currentLocalpos = Mathf.Clamp(currentLocalpos, minDistance, maxDistance);
            Vector2 newPosition = new Vector2(currentLocalpos, 0);

            currentRoutine = StartCoroutine(SmoothMoveLocal(transform.localPosition, newPosition, easing));
            //Debug.Log("position = " + newPosition);
        }
        else        //Required distance is not moved
        {
            if (currentRoutine != null)
                StopCoroutine(currentRoutine);

            Vector2 prevPosition = new Vector2(currentLocalpos, 0);
            currentRoutine = StartCoroutine(SmoothMoveLocal(transform.localPosition, prevPosition, easing));
        }

        SetUIElements();
        //onDrag = false;
    }

    IEnumerator SmoothMoveLocal(Vector2 startPosition, Vector2 endPosition, float time)
    {
        float t = 0f;
        while (t <= 1f)
        {
            t += Time.deltaTime / time;
            transform.localPosition = Vector2.Lerp(startPosition, endPosition, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }

    void SetUIElements()
    {
        if(currentLocalpos == headUIPosition)
        {
            SetUIElements(AttireType.Head);
        }
        else if(currentLocalpos == upperUIPosition)
        {
            SetUIElements(AttireType.Upper);
        }
        else if(currentLocalpos == lowerUIPosition)
        {
            SetUIElements(AttireType.Lower);
        }
    }

    void SetUIElements(AttireType type)
    {
        switch(type)
        {
            case AttireType.Head:
                headArrow.SetActive(true);
                headPoint.SetActive(true);
                upperArrow.SetActive(false);
                upperPoint.SetActive(false);
                lowerArrow.SetActive(false);
                lowerPoint.SetActive(false);
                break;

            case AttireType.Upper:
                headArrow.SetActive(false);
                headPoint.SetActive(false);
                upperArrow.SetActive(true);
                upperPoint.SetActive(true);
                lowerArrow.SetActive(false);
                lowerPoint.SetActive(false);
                break;

            case AttireType.Lower:
                headArrow.SetActive(false);
                headPoint.SetActive(false);
                upperArrow.SetActive(false);
                upperPoint.SetActive(false);
                lowerArrow.SetActive(true);
                lowerPoint.SetActive(true);
                break;
        }
        ShopPanel.instance.ChangeCost(type);
    }

    public void SetPage(int id)
    {
        if (id == 1)
        {
            if (currentRoutine != null)
                StopCoroutine(currentRoutine);

            currentLocalpos = headUIPosition;
            currentRoutine = StartCoroutine(SmoothMoveLocal(transform.localPosition, new Vector2(currentLocalpos, 0), easing));
            SetUIElements(AttireType.Head);
        }
        else if (id == 2)
        {
            if (currentRoutine != null)
                StopCoroutine(currentRoutine);

            currentLocalpos = upperUIPosition;
            currentRoutine = StartCoroutine(SmoothMoveLocal(transform.localPosition, new Vector2(currentLocalpos, 0), easing));
            SetUIElements(AttireType.Upper);
        }
        else if (id == 3)
        {
            if (currentRoutine != null)
                StopCoroutine(currentRoutine);

            currentLocalpos = lowerUIPosition;
            currentRoutine = StartCoroutine(SmoothMoveLocal(transform.localPosition, new Vector2(currentLocalpos, 0), easing));
            SetUIElements(AttireType.Lower);
        }
    }

    public enum AttireType
    {
        Head,
        Upper,
        Lower
    }
}
