﻿using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    [SerializeField] Image fillerImg = null;
    [SerializeField] Text remainingPlatesText = null;
    Animator remtextAnim = null;

    public static int capturedPlates = 0;
    int temp = 0; //Stores the old captured plate value

    void Start()
    {
        capturedPlates = 0;
        temp = 0;
        fillerImg.fillAmount = 0;

        remainingPlatesText.text = LevelController.instance.TotalPlates.ToString();
        remtextAnim = remainingPlatesText.GetComponent<Animator>();
    } 

    public static void increasePlates(int platesCount)
    {
        capturedPlates = platesCount;
    }

    private void Update()
    {
        #region Filler Image Update
        float count = (float)capturedPlates /LevelController.instance.TotalPlates;

        fillerImg.fillAmount = Mathf.Lerp(fillerImg.fillAmount, count, .1f);

        if(fillerImg.fillAmount >= .98f)
        {
            //Game won code here
            GameManager.instance.GameWon();
        }
        #endregion

        int dif = LevelController.instance.TotalPlates - capturedPlates;

        if(temp != capturedPlates)
        {
            if(dif >= 0)
            {
                remainingPlatesText.text = dif.ToString();
                remtextAnim.Play("Open");
                temp = capturedPlates;
            }
            else
            {
                remainingPlatesText.text = "0";
                remtextAnim.Play("Open");
                temp = capturedPlates;
            }
        }
    }
}
