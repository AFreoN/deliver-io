﻿using UnityEngine;
using UnityEngine.UI;

public class TutorialPanel : MonoBehaviour
{
    [SerializeField] GameObject[] tutorialImages = null;

    [Header("Buttons")]
    [SerializeField] Button rightButton = null;
    [SerializeField] Button leftButton = null;
    [SerializeField] Button startButton = null;

    int currentIndex = 0;

    private void Start()
    {
        ActiveTut(currentIndex);

        rightButton.interactable = true;
        leftButton.interactable = false;
        startButton.interactable = false;
    }

    void ActiveTut(int index)
    {
        if (index > tutorialImages.Length - 1)
            return;

        foreach (GameObject g in tutorialImages)
        {
            g.SetActive(false);
        }
        tutorialImages[index].SetActive(true);
    }

    public void MoveRight()
    {
        currentIndex++;
        ActiveTut(currentIndex);
        if(currentIndex  >= tutorialImages.Length - 1)
        {
            rightButton.interactable = false;
            startButton.interactable = true;
        }

        if(currentIndex > 0)
        {
            leftButton.interactable = true;
        }
    }

    public void MoveLeft()
    {
        currentIndex--;
        currentIndex = currentIndex < 0 ? 0 : currentIndex;
        ActiveTut(currentIndex);
        if(currentIndex <= 0)
        {
            leftButton.interactable = false;
            rightButton.interactable = true;
        }
    }

    public void StartGame()
    {
        //Game Start Code here
        GameManager.instance.ForceStartGame();
    }
}
