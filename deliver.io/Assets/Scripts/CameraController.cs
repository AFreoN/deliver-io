﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static Camera cam { get; private set; }
    public static CameraController current { get; private set; }

    [SerializeField] Transform target = null;
    [SerializeField][Range(0,1)] float LerpSpeed = .2f;


    Vector3 offset = Vector3.zero;
    Vector3 startOffset = Vector3.zero;

    private void Awake()
    {
        current = this;
        cam = GetComponent<Camera>();
    }
    void Start()
    {
        offset = transform.position - target.position;
        startOffset = offset;
    }

    private void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, target.position + offset, LerpSpeed);
    }

    public void ZoomOut(float scale)
    {
        offset = startOffset * (1 + (scale - 1) * .6f);
    }

    public void GoDefault()
    {
        offset = startOffset;
    }
}
