﻿using UnityEngine;

public class DragToMove : MonoBehaviour
{
    public static GameObject instanceGo;

    private void Awake()
    {
        if (Camera.main != null)
            gameObject.GetComponent<Canvas>().worldCamera = Camera.main;
        instanceGo = gameObject;
    }
}
