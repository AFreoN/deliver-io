﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneReset : MonoBehaviour
{
    public GameObject[] destroyableObjects = null;

    private void Awake()
    {
        if (SceneManager.GetActiveScene().buildIndex != 0)
            SceneManager.LoadScene(0);

        foreach (GameObject g in destroyableObjects)
            Destroy(g);

        Destroy(gameObject);
    }
}
