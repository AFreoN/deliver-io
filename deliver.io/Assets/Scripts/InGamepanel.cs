﻿using UnityEngine;
using UnityEngine.UI;

public class InGamepanel : MonoBehaviour
{
    public static InGamepanel instance { get; private set; }

    [SerializeField] Text coinText = null;
    Animator IGCoinAnim = null;
    public const int coinPerPlate = 5;
    [SerializeField][Range(0,1)] float coinSpawnDistance = .2f;

    [Header("Prefab")]
    [SerializeField] Transform coinPrefab = null;
    int coins = 0;

    Transform canvas = null;
    Camera cam = null;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        coins = DataManager.Coins;
        coinText.text = coins.ToString();
        IGCoinAnim = coinText.GetComponent<Animator>();

        canvas = transform.parent;
        cam = Camera.main;
    }

    public void AddCoin()
    {
        DataManager.SaveCoins(1);
        coins++;
        coinText.text = coins.ToString();
    }

    public void SpawnCoin(Vector3 worldPos)
    {
        AdRewards.currentCoins += coinPerPlate;
        //Debug.Log("Current Conis = " + AdRewards.currentCoins);
        Vector2 screenPos = cam.ScreenToWorldPoint(worldPos);
        
        for(int i = 0; i < coinPerPlate; i++)
        {
            Transform t = Instantiate(coinPrefab, screenPos, Quaternion.identity);
            t.SetParent(canvas);
            t.localScale = Vector3.one;
            t.localPosition = screenPos + Random.insideUnitCircle * coinSpawnDistance * Screen.width;
            Lerper.CreateLinearLerp(t, coinText.transform.parent.GetChild(1), Random.Range(1,1.5f), () => { AddCoin(); IGCoinAnim.Play("Open"); Destroy(t.gameObject); });
        }
    }
}
