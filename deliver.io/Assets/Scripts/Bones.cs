﻿using UnityEngine;

public class Bones : MonoBehaviour
{
    [SerializeField] Transform[] bones = null;

    public static Transform[] allBones { get; private set; }

    private void Awake()
    {
        allBones = bones;
    }
}
