﻿using UnityEngine;
using UnityEngine.UI;

public class AdRewards : MonoBehaviour
{
    public static int currentCoins = 0;
    const int rewardCoins = 10;
    const float coinSpawnDistance = .1f;

    [Header("Main")]
    [SerializeField] Transform canvas = null;
    [SerializeField] Transform coinPrefab = null;

    [Header("Double Coins")]
    [SerializeField] Button doubleCoinsBtn = null;
    [SerializeField] Button doubleCoinsBtn2 = null;
    [SerializeField] RectTransform ingameCoinImg = null;

    [Header("Shop Coins")]
    [SerializeField] RectTransform shopCoinImg = null;
    [SerializeField] RectTransform shopAdBtn = null;

    private void Awake()
    {
        currentCoins = 0;
        doubleCoinsBtn.interactable = true;
        doubleCoinsBtn2.interactable = true;
    }

    private void Start()
    {
        IronSourceEvents.onRewardedVideoAdRewardedEvent += GiveRewards;
    }

    public void OnDoubleCoinClick()
    {
        Advert_Manager.instance.showRewardedVideo(Advert_Manager.doubleCoinsReward);
        doubleCoinsBtn.interactable = false;
    }

    public void RewardDoubleCoins()
    {
        doubleCoinsBtn.interactable = false;
        Destroy(doubleCoinsBtn.gameObject.GetComponent<Animator>());
        doubleCoinsBtn.transform.localScale = Vector3.one;

        doubleCoinsBtn2.interactable = false;
        Destroy(doubleCoinsBtn2.gameObject.GetComponent<Animator>());
        doubleCoinsBtn2.transform.localScale = Vector3.one;

        DataManager.SaveCoins(currentCoins);
        SpawnCoinsGameWon(currentCoins);
    }

    public void OnShopAdClick()
    {
        Advert_Manager.instance.showRewardedVideo(Advert_Manager.shopCoinReward);
    }

    public void ShopAdReward()
    {
        DataManager.SaveCoins(rewardCoins);
        ShopPanel.instance.coinText.text = DataManager.Coins.ToString();
        SpawnCoinsShop(rewardCoins);
    }

    public void SpawnCoinsGameWon(int count)
    {
        Camera cam = CameraController.cam;

        Vector2 screenPos = doubleCoinsBtn.transform.position;

        for (int i = 0; i < count; i++)
        {
            Transform t = Instantiate(coinPrefab, screenPos, Quaternion.identity);
            t.SetParent(canvas);
            t.localScale = Vector3.one;
            t.position = screenPos + Random.insideUnitCircle * coinSpawnDistance * Screen.width;
            Lerper.CreateLinearLerp(t, ingameCoinImg, Random.Range(1, 1.5f), () => { Destroy(t.gameObject); });
        }
    }

    public void SpawnCoinsShop(int count)
    {
        Camera cam = CameraController.cam;

        Vector2 screenPos = shopAdBtn.position;

        for (int i = 0; i < count; i++)
        {
            Transform t = Instantiate(coinPrefab, screenPos, Quaternion.identity);
            t.SetParent(canvas);
            t.localScale = Vector3.one;
            t.position = screenPos + Random.insideUnitCircle * coinSpawnDistance * Screen.width;
            Lerper.CreateLinearLerp(t, shopCoinImg, Random.Range(1, 1.5f), () => { Destroy(t.gameObject); });
        }
    }

    public void GiveRewards(IronSourcePlacement ssp)
    {
        switch(ssp.getPlacementName())
        {
            case Advert_Manager.doubleCoinsReward:
                RewardDoubleCoins();
                break;

            case Advert_Manager.shopCoinReward:
                ShopAdReward();
                break;

            default:
                RewardDoubleCoins();
                break;
        }
    }
}
