﻿using UnityEngine;
using System.Collections.Generic;

public class Skinner : MonoBehaviour
{
    public SkinnedMeshRenderer targetRenderer;

    void Start()
    {
        Dictionary<string, Transform> boneMap = new Dictionary<string, Transform>();
        foreach (Transform bone in targetRenderer.bones)
        {
            boneMap[bone.name] = bone;
        }

        SkinnedMeshRenderer thisRenderer = GetComponent<SkinnedMeshRenderer>();
        Transform[] boneArray = thisRenderer.bones;
        for (int i = 0; i < boneArray.Length; ++i)
        {
            string boneName = boneArray[i].name;
            if (false == boneMap.TryGetValue(boneName, out boneArray[i]))
            {
                Debug.LogError("failed to get bone: " + boneName);
                Debug.Break();
            }
        }

        Vector3 localScale = transform.localScale;
        transform.parent = targetRenderer.transform.parent;
        transform.localScale = localScale;
        thisRenderer.rootBone = transform.parent.GetChild(0);
        thisRenderer.bones = boneArray; //take effect
    }
}
