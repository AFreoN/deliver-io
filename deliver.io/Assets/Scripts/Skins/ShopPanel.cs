﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class ShopPanel : MonoBehaviour
{
    public static ShopPanel instance { get; private set; }

    const int iconID = 1;
    const int lockID = 2;
    #region Cost
    public const int HeadCost = 40;
    public const int UpperCost = 60;
    public const int LowerCost = 60;
    #endregion

    const float switchTime = .4f;

    [SerializeField] AssignSkin skinHandler = null;
    PageDrag.AttireType attireType = PageDrag.AttireType.Head;

    [Header("Head UI")]
    [SerializeField] Transform headSelectionFrame = null;
    [SerializeField] Transform headTickMark = null;
    [SerializeField] Transform headGearContent = null;

    [Header("Upper UI")]
    [SerializeField] Transform upperSelectionFrame = null;
    [SerializeField] Transform upperTickMark = null;
    [SerializeField] Transform upperGearContent = null;

    [Header("Lower UI")]
    [SerializeField] Transform lowerSelectionFrame = null;
    [SerializeField] Transform lowerTickMark = null;
    [SerializeField] Transform lowerGearContent = null;

    [Header("Icons & Locks")]
    GameObject[] headIcons = null;
    GameObject[] headLocks = null;
    GameObject[] upperIcons = null;
    GameObject[] upperLocks = null;
    GameObject[] lowerIcons = null;
    GameObject[] lowerLocks = null;

    [Header("Text & Buttons")]
    public Text coinText = null;
    [SerializeField] Button buyBtn = null;
    Text buyCoinsText = null;

    private void OnEnable()
    {
        if (buyCoinsText != null)
        {
            buyCoinsText.text = HeadCost.ToString();
            bool b = false;
            foreach(int i in DataManager.loadHeadSkinData())
            {
                if (i == 0)     //Check if atleast one is locked
                {
                    b = true;
                    break;
                }
            }
            buyBtn.interactable = b;
            attireType = PageDrag.AttireType.Head;
        }
    }

    private void Awake()
    {
        instance = this;
        buyCoinsText = buyBtn.transform.GetChild(0).GetChild(0).GetComponent<Text>();
        coinText.text = DataManager.Coins.ToString();
    }

    private void Start()
    {
        headSelectionFrame.gameObject.SetActive(false);
        headTickMark.gameObject.SetActive(true);

        upperSelectionFrame.gameObject.SetActive(false);
        upperTickMark.gameObject.SetActive(true);

        lowerSelectionFrame.gameObject.SetActive(false);
        lowerTickMark.gameObject.SetActive(true);

        UpdateHeadSkinUI();
        UpdateUpperSkinUI();
        UpdateLowerSkinUI();
    }

    public void ChangeCost(PageDrag.AttireType type)
    {
        attireType = type;
        int cost = 0;
        switch(type)
        {
            case PageDrag.AttireType.Head:
                buyCoinsText.text = HeadCost.ToString();
                bool b = false;
                foreach (int i in DataManager.loadHeadSkinData())
                {
                    if (i == 0)     //Check if atleast one is locked
                    {
                        b = true;
                        break;
                    }
                }
                buyBtn.interactable = b;
                cost = HeadCost;
                break;

            case PageDrag.AttireType.Upper:
                buyCoinsText.text = UpperCost.ToString();
                b = false;
                foreach (int i in DataManager.loadUpperSkinData())
                {
                    if (i == 0)     //Check if atleast one is locked
                    {
                        b = true;
                        break;
                    }
                }
                buyBtn.interactable = b;
                cost = UpperCost;
                break;

            case PageDrag.AttireType.Lower:
                buyCoinsText.text = LowerCost.ToString();
                b = false;
                foreach (int i in DataManager.loadLowerSkinData())
                {
                    if (i == 0)     //Check if atleast one is locked
                    {
                        b = true;
                        break;
                    }
                }
                buyBtn.interactable = b;
                cost = LowerCost;
                break;
        }
        buyBtn.interactable = DataManager.Coins >= cost;
    }

    void UpdateHeadSkinUI()
    {
        int headCount = headGearContent.childCount;
        headIcons = new GameObject[headCount];
        for (int i = 0; i < headCount; i++)
            headIcons[i] = headGearContent.GetChild(i).GetChild(iconID).gameObject;

        headLocks = new GameObject[headCount];
        for (int i = 0; i < headCount; i++)
            headLocks[i] = headGearContent.GetChild(i).GetChild(lockID).gameObject;

        //Enable Disable Head Icons
        int[] h = DataManager.loadHeadSkinData();   // 0 = locked, 1 = unlocked
        for (int i = 0; i < h.Length; i++)
        {
            headIcons[i].SetActive(h[i] == 1);
            headLocks[i].SetActive(h[i] == 0);
        }

        headTickMark.SetParent(headGearContent.GetChild(DataManager.HeadGear));
        headTickMark.localPosition = Vector3.zero;
    }

    void UpdateUpperSkinUI()
    {
        int upperCount = upperGearContent.childCount;
        upperIcons = new GameObject[upperCount];
        for (int i = 0; i < upperCount; i++)
            upperIcons[i] = upperGearContent.GetChild(i).GetChild(iconID).gameObject;

        upperLocks = new GameObject[upperCount];
        for (int i = 0; i < upperCount; i++)
            upperLocks[i] = upperGearContent.GetChild(i).GetChild(lockID).gameObject;

        //Enable Disable Head Icons
        int[] h = DataManager.loadUpperSkinData();   // 0 = locked, 1 = unlocked
        for (int i = 0; i < h.Length; i++)
        {
            upperIcons[i].SetActive(h[i] == 1);
            upperLocks[i].SetActive(h[i] == 0);
        }

        upperTickMark.SetParent(upperGearContent.GetChild(DataManager.UpperGear));
        upperTickMark.localPosition = Vector3.zero;
    }

    void UpdateLowerSkinUI()
    {
        int lowerCount = lowerGearContent.childCount;
        lowerIcons = new GameObject[lowerCount];
        for (int i = 0; i < lowerCount; i++)
            lowerIcons[i] = lowerGearContent.GetChild(i).GetChild(iconID).gameObject;

        lowerLocks = new GameObject[lowerCount];
        for (int i = 0; i < lowerCount; i++)
            lowerLocks[i] = lowerGearContent.GetChild(i).GetChild(lockID).gameObject;

        //Enable Disable Head Icons
        int[] h = DataManager.loadLowerSkinData();   // 0 = locked, 1 = unlocked
        for (int i = 0; i < h.Length; i++)
        {
            lowerIcons[i].SetActive(h[i] == 1);
            lowerLocks[i].SetActive(h[i] == 0);
        }

        lowerTickMark.SetParent(lowerGearContent.GetChild(DataManager.LowerGear));
        lowerTickMark.localPosition = Vector3.zero;
    }

    //called from the AssignSkin script, while pressing skin button directly
    public void SetheadTickMark()
    {
        headTickMark.SetParent(headGearContent.GetChild(DataManager.HeadGear));
        headTickMark.localPosition = Vector3.zero;
    }

    public void SetUpperTickMark()
    {
        upperTickMark.SetParent(upperGearContent.GetChild(DataManager.UpperGear));
        upperTickMark.localPosition = Vector3.zero;
    }

    public void SetLowerTickMark()
    {
        lowerTickMark.SetParent(lowerGearContent.GetChild(DataManager.LowerGear));
        lowerTickMark.localPosition = Vector3.zero;
    }

    //This function is called from the buy button
    public void UnlockSkinBuyBtn()
    {
        if (attireType == PageDrag.AttireType.Head)
            headUnlockSkin();
        else if (attireType == PageDrag.AttireType.Upper)
            upperUnlockSkin();
        else
            lowerUnlockSkin();
    }

    void headUnlockSkin()
    {
        if (DataManager.Coins < HeadCost)
            return;

        headSelectionFrame.gameObject.SetActive(true);
        List<Transform> availableSkins = new List<Transform>();
        int[] h = DataManager.loadHeadSkinData();
        for(int i = 0; i < headGearContent.childCount; i++)
        {
            if (h[i] == 0)
                availableSkins.Add(headGearContent.GetChild(i));
        }

        DataManager.SaveCoins(-HeadCost);
        coinText.text = DataManager.Coins.ToString();
        buyBtn.interactable = false;
        StartCoroutine(startSwitchingFrame(availableSkins, 10, headSelectionFrame, PageDrag.AttireType.Head));
    }

    void upperUnlockSkin()
    {
        if (DataManager.Coins < UpperCost)
            return;

        upperSelectionFrame.gameObject.SetActive(true);
        List<Transform> availableSkins = new List<Transform>();
        int[] h = DataManager.loadUpperSkinData();
        for (int i = 0; i < upperGearContent.childCount; i++)
        {
            if (h[i] == 0)
                availableSkins.Add(upperGearContent.GetChild(i));
        }

        DataManager.SaveCoins(-UpperCost);
        coinText.text = DataManager.Coins.ToString();
        buyBtn.interactable = false;
        StartCoroutine(startSwitchingFrame(availableSkins, 10, upperSelectionFrame, PageDrag.AttireType.Upper));
    }

    void lowerUnlockSkin()
    {
        if (DataManager.Coins < LowerCost)
            return;

        lowerSelectionFrame.gameObject.SetActive(true);
        List<Transform> availableSkins = new List<Transform>();
        int[] h = DataManager.loadLowerSkinData();
        for (int i = 0; i < lowerGearContent.childCount; i++)
        {
            if (h[i] == 0)
                availableSkins.Add(lowerGearContent.GetChild(i));
        }

        DataManager.SaveCoins(-LowerCost);
        coinText.text = DataManager.Coins.ToString();
        buyBtn.interactable = false;
        StartCoroutine(startSwitchingFrame(availableSkins, 10, lowerSelectionFrame, PageDrag.AttireType.Lower));
    }

    IEnumerator startSwitchingFrame(List<Transform> availableSkins, int count, Transform selectionFrame, PageDrag.AttireType type)
    {
        if (availableSkins.Count == 0)
            yield return null;
        else if (availableSkins.Count == 1)
            count = 2;
        else
            availableSkins = randomizeList(availableSkins);

        float t = 0;
        int c = 0;
        selectionFrame.gameObject.SetActive(true);
        while(c < count)
        {
            t += Time.deltaTime / switchTime;
            if(t > switchTime)
            {
                t = 0;
                c++;
                selectionFrame.SetParent(availableSkins[c % availableSkins.Count]);
                selectionFrame.localPosition = Vector3.zero;
                if (c >= count)
                {
                    switch(type)
                    {
                        case PageDrag.AttireType.Head:
                            UnlockHead(availableSkins[c % availableSkins.Count].GetSiblingIndex());
                            break;
                        case PageDrag.AttireType.Upper:
                            UnlockUpper(availableSkins[c % availableSkins.Count].GetSiblingIndex());
                            break;
                        case PageDrag.AttireType.Lower:
                            UnlockLower(availableSkins[c % availableSkins.Count].GetSiblingIndex());
                            break;
                    }
                }
                    //UnlockHead(int.Parse(availableSkins[c%availableSkins.Count].name));
            }
            yield return null;
        }
    }

    List<Transform> randomizeList(List<Transform> skinList)
    {
        List<Transform> result = new List<Transform>();
        List<int> id = new List<int>();
        for (int i = 0; i < skinList.Count; i++)
        {
            id.Add(i);
            result.Add(skinList[i]);
        }

        for(int i = 0; i < skinList.Count; i++)
        {
            int r = Random.Range(0, id.Count);
            result[i] = skinList[id[r]];
            id.RemoveAt(r);
        }

        return result;
    }

    void UnlockHead(int id)
    {
        DataManager.unlockHeadSkin(id);
        skinHandler.assignHeadGear(id);
        UpdateHeadSkinUI();

        headSelectionFrame.gameObject.SetActive(false);
        bool b = false;
        foreach(int i in DataManager.loadHeadSkinData())
        {
            if(i == 0)  //Check if atleast one of the skin is locked
            {
                b = true;
                break;
            }
        }
        buyBtn.interactable = b;
    }

    void UnlockUpper(int id)
    {
        DataManager.unlockUpperSkin(id);
        skinHandler.assignUpperGear(id);
        UpdateUpperSkinUI();

        upperSelectionFrame.gameObject.SetActive(false);
        bool b = false;
        foreach (int i in DataManager.loadUpperSkinData())
        {
            if (i == 0)  //Check if atleast one of the skin is locked
            {
                b = true;
                break;
            }
        }
        buyBtn.interactable = b;
    }

    void UnlockLower(int id)
    {
        DataManager.unlockLowerSkin(id);
        skinHandler.assignLowerGear(id);
        UpdateLowerSkinUI();

        lowerSelectionFrame.gameObject.SetActive(false);
        bool b = false;
        foreach (int i in DataManager.loadLowerSkinData())
        {
            if (i == 0)  //Check if atleast one of the skin is locked
            {
                b = true;
                break;
            }
        }
        buyBtn.interactable = b;
    }
}
