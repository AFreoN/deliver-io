﻿using UnityEngine;

public class AssignSkin : MonoBehaviour
{
    public static AssignSkin instance { get; private set; }

    [Header("Player in Game")]
    [SerializeField] SkinnedMeshRenderer playerSkin = null;
    [SerializeField] Transform player = null;
    [SerializeField] Transform playerRootBone = null;

    [Header("Player in Shop")]
    [SerializeField] SkinnedMeshRenderer referenceSkin = null;
    [SerializeField] Transform shopPlayer = null;
    [SerializeField] Transform shopPlayerRootBone = null;

    [Header("Skins")]
    public SkinnedMeshRenderer[] headGears;
    public SkinnedMeshRenderer[] upperGears;
    public SkinnedMeshRenderer[] lowerGears;

    SkinnedMeshRenderer currentHead = null;
    SkinnedMeshRenderer currentUpper = null;
    SkinnedMeshRenderer currentLower = null;

    private void Awake()
    {
        instance = this;
    }

    private void OnDisable()
    {
        instance = this;
    }

    private void OnEnable()
    {
        instance = this;

        currentHead = AssignHeadGear(DataManager.HeadGear);
        currentUpper = AssignUpperGear(DataManager.UpperGear);
        currentLower = AssignLowerGear(DataManager.LowerGear);
    }

    public void AssignToPlayer()
    {
        AssignHeadGear(DataManager.HeadGear, true);
        AssignUpperGear(DataManager.UpperGear, true);
        AssignLowerGear(DataManager.LowerGear, true);
    }

    SkinnedMeshRenderer AssignHeadGear(int id, bool onPlayer = false)
    {
        id = id >= headGears.Length ? 0 : id;
        DataManager.SaveHeadGear(id);
        SkinnedMeshRenderer s = headGears[id];

        if (s == null)
        {
            if (currentHead != null)
                currentHead.gameObject.SetActive(false);

            currentHead = s;
            return null;
        }

        if (currentHead != null)
            currentHead.gameObject.SetActive(false);

        s.gameObject.SetActive(true);
        if(onPlayer)
        {
            s.transform.parent = player;
            s.bones = playerSkin.bones;
            s.rootBone = playerRootBone;
        }
        else
        {
            s.transform.parent = shopPlayer;
            s.bones = referenceSkin.bones;
            s.rootBone = shopPlayerRootBone;
        }
        currentHead = s;
        return s;
    }

    SkinnedMeshRenderer AssignUpperGear(int id, bool onPlayer = false)
    {
        id = id >= upperGears.Length ? 0 : id;
        DataManager.SaveUpperGear(id);
        SkinnedMeshRenderer s = upperGears[id];

        if (s == null)
        {
            if (currentUpper != null)
                currentUpper.gameObject.SetActive(false);

            currentUpper = s;
            return null;
        }

        if (currentUpper != null)
            currentUpper.gameObject.SetActive(false);

        s.gameObject.SetActive(true);
        if (onPlayer)
        {
            s.transform.parent = player;
            s.bones = playerSkin.bones;
            s.rootBone = playerRootBone;
        }
        else
        {
            s.transform.parent = shopPlayer;
            s.bones = referenceSkin.bones;
            s.rootBone = shopPlayerRootBone;
        }
        currentUpper = s;
        return s;
    }

    SkinnedMeshRenderer AssignLowerGear(int id, bool onPlayer = false)
    {
        id = id >= lowerGears.Length ? 0 : id;
        DataManager.SaveLowerGear(id);
        SkinnedMeshRenderer s = lowerGears[id];

        if (s == null)
        {
            if (currentLower != null)
                currentLower.gameObject.SetActive(false);

            currentLower = s;
            return null;
        }

        if (currentLower != null)
            currentLower.gameObject.SetActive(false);

        s.gameObject.SetActive(true);
        if (onPlayer)
        {
            s.transform.parent = player;
            s.bones = playerSkin.bones;
            s.rootBone = playerRootBone;
        }
        else
        {
            s.transform.parent = shopPlayer;
            s.bones = referenceSkin.bones;
            s.rootBone = shopPlayerRootBone;
        }
        currentLower = s;
        return s;
    }

    public void assignHeadGear(int id)
    {
        if (DataManager.isHeadSkinUnlocked(id) == false)
            return;

        AssignHeadGear(id);
        ShopPanel.instance.SetheadTickMark();
    }

    //This function is called while pressing the skin button directly
    public void assignUpperGear(int id)
    {
        if (DataManager.isUpperSkinUnlocked(id) == false)
            return;

        AssignUpperGear(id);
        ShopPanel.instance.SetUpperTickMark();
    }

    public void assignLowerGear(int id)
    {
        if (DataManager.isLowerSkinUnlocked(id) == false)
            return;

        AssignLowerGear(id);
        ShopPanel.instance.SetLowerTickMark();
    }
}
